package basicTdd;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


/**
 * Boatengs house in the middle
 *  -------------
 *  | 0 | 1 | 2 |
 *  -------------
 *  | 3 |[4]| 5 |
 *  -------------
 *  | 6 | 7 | 8 |
 *  -------------
 *
 *      Width = 10
 *  -----------------
 *  |        |      |
 *  |  - Pos(0,0) - | Height = 10
 *  |        |      |
 *  -----------------
 */

public class VillaTest {

    private Villa boateng;

    @Before
    public void setup() {
        boateng = new Villa(0, 0);
    }

    // 1
    @Test
    public void testIsNeighbourLeft() {
        // prepare
        Villa neighbour = new Villa(boateng.getPosX() - Villa.WIDTH, boateng.getPosY());

        // run
        boolean isNeighbour = boateng.isNeighbour(neighbour);

        // assert
        assertTrue(isNeighbour);
    }

    @Test
    public void testIsNeighbourShouldReturnFalseForTooFarVillaXLeftPos() {
        // prepare
        Villa neighbour = new Villa(boateng.getPosX() - 2 * Villa.WIDTH, boateng.getPosY());

        // run
        boolean isNeighbour = boateng.isNeighbour(neighbour);

        // assert
        assertFalse(isNeighbour);
    }

    @Test
    public void testIsNeighbourRight() {
        // prepare
        Villa neighbour = new Villa(boateng.getPosX() + Villa.WIDTH, boateng.getPosY());

        // run
        boolean isNeighbour = boateng.isNeighbour(neighbour);

        // assert
        assertTrue(isNeighbour);
    }

    @Test
    public void testIsNeighbourShouldReturnFalseForTooFarVillaXRightPos() {
        // prepare
        Villa neighbour = new Villa(boateng.getPosX() + 2 * Villa.WIDTH, boateng.getPosY());

        // run
        boolean isNeighbour = boateng.isNeighbour(neighbour);

        // assert
        assertFalse(isNeighbour);
    }

    @Test
    public void testIsNeighbourTop() {
        // prepare
        Villa neighbour = new Villa(boateng.getPosX(), boateng.getPosY() + Villa.HEIGTH);

        // run
        boolean isNeighbour = boateng.isNeighbour(neighbour);

        // assert
        assertTrue(isNeighbour);
    }

    @Test
    public void testIsNeighbourShouldReturnFalseForTooFarVillaYTopPos() {
        // prepare
        Villa neighbour = new Villa(boateng.getPosX(), boateng.getPosY() + 2 * Villa.HEIGTH);

        // run
        boolean isNeighbour = boateng.isNeighbour(neighbour);

        // assert
        assertFalse(isNeighbour);
    }

    @Test
    public void testIsNeighbourBottom() {
        // prepare
        Villa neighbour = new Villa(boateng.getPosX(), boateng.getPosY() - Villa.HEIGTH);

        // run
        boolean isNeighbour = boateng.isNeighbour(neighbour);

        // assert
        assertTrue(isNeighbour);
    }

    @Test
    public void testIsNeighbourShouldReturnFalseForTooFarVillaYBottomPos() {
        // prepare
        Villa neighbour = new Villa(boateng.getPosX(), boateng.getPosY() - 2 * Villa.HEIGTH);

        // run
        boolean isNeighbour = boateng.isNeighbour(neighbour);

        // assert
        assertFalse(isNeighbour);
    }

    // Diagonal tests
    @Test
    public void testIsNeighbourShouldReturnFalseForTopLeftPos() {
        // prepare
        Villa neighbour = new Villa(boateng.getPosX() - Villa.WIDTH, boateng.getPosY() + Villa.HEIGTH);

        // run
        boolean isNeighbour = boateng.isNeighbour(neighbour);

        // assert
        assertFalse(isNeighbour);
    }

    @Test
    public void testIsNeighbourShouldReturnFalseForTopRightPos() {
        // prepare
        Villa neighbour = new Villa(boateng.getPosX() + Villa.WIDTH, boateng.getPosY() + Villa.HEIGTH);

        // run
        boolean isNeighbour = boateng.isNeighbour(neighbour);

        // assert
        assertFalse(isNeighbour);
    }

    @Test
    public void testIsNeighbourShouldReturnFalseForBottomLeftPos() {
        // prepare
        Villa neighbour = new Villa(boateng.getPosX() - Villa.WIDTH, boateng.getPosY() - Villa.HEIGTH);

        // run
        boolean isNeighbour = boateng.isNeighbour(neighbour);

        // assert
        assertFalse(isNeighbour);
    }

    @Test
    public void testIsNeighbourShouldReturnFalseForBottomRightPos() {
        // prepare
        Villa neighbour = new Villa(boateng.getPosX() + Villa.WIDTH, boateng.getPosY() - Villa.HEIGTH);

        // run
        boolean isNeighbour = boateng.isNeighbour(neighbour);

        // assert
        assertFalse(isNeighbour);
    }

}
