package parametrized;

import org.junit.Test;
import org.junit.runners.Parameterized;
import paramtrized.SomeClass;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;


public class SomeClassTest {

    // Has to be public
    @Parameterized.Parameter
    public int parameter1;
    @Parameterized.Parameter(value = 1)
    public int parameter2;

    // creates the test data
    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                {1, 2},
                {3, 4},
                {-20, 0}});
    }

    @Test
    public void testAdd() {
        SomeClass classUnderTest = new SomeClass();
        assertEquals(parameter1 + parameter2, classUnderTest.add(parameter1, parameter2));
    }

}