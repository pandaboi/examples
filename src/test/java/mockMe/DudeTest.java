package mockMe;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * 1. local mock
 *
 * 2. public mock
 */
@RunWith(MockitoJUnitRunner.class) // 2. populate the annotated fields
public class DudeTest {

    // 2.
    @Mock
    public Drink drink;

    @Test
    public void testMakeWaterToWine() {
        // prepare
        //Drink drink = new Drink("Water");
//        Drink drink = mock(Drink.class); // 1.
        Dude dudeUndertest = new Dude(drink);

        // run
        dudeUndertest.makeWaterToWine();

        // verify
        verify(drink, times(1)).setName("Wine");
    }
}