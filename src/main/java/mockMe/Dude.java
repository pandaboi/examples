package mockMe;

public class Dude {

    private Drink drink;

    public Dude(Drink drink) {
        this.drink = drink;
    }

    public void makeWaterToWine() {
        drink.setName("Wine");
    }
}
