package basicTdd;

public class Villa {

    public static final int WIDTH = 10;
    public static final int HEIGTH = 10;
    private int posX;
    private int posY;

    public Villa(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    // 0.
//    public boolean isNeighbour(Villa villa) {
//        return false;
//    }

// 1.
//    public boolean isNeighbour(Villa villa) {
//        return true;
//    }

//     2. left
//    public boolean isNeighbour(Villa villa) {
//        if (posX - WIDTH == villa.getPosX()) {
//            return true;
//        }
//
//        return false;
//    }

    //     2. left + right
//    public boolean isNeighbour(Villa villa) {
//        if (posX - WIDTH == villa.getPosX() || posX + WIDTH == villa.getPosX())  {
//            return true;
//        }
//
//        if (posY - HEIGTH == villa.getPosY() || posY + HEIGTH == villa.getPosY()) {
//            return true;
//        }
//
//        return false;
//    }

    // Diagonales
    // refactoren
    public boolean isNeighbour(Villa villa) {
        if ((posX - WIDTH == villa.getPosX() || posX + WIDTH == villa.getPosX()) && posY == villa.getPosY())  {
            return true;
        }

        if ((posY - HEIGTH == villa.getPosY() || posY + HEIGTH == villa.getPosY()) && posX == villa.getPosX()) {
            return true;
        }

        return false;
    }
}
